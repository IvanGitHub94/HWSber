package HW3_3.Task2;

public class BestCarpenterEver {

    public static void main(String[] args) {
        Mebel taburetka = new Taburetka();
        Mebel stol = new Stol();

        System.out.println(repairMebel(taburetka));
        System.out.println(repairMebel(stol));
    }

    public static boolean repairMebel(Mebel mebel) {
        return mebel instanceof Repairable;
    }
}
