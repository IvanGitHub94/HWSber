package HW3_3.Task3;

import java.util.ArrayList;
import java.util.Scanner;

public class T_3 {
    public static void main(String[] args) {
        int columns;
        int rows;

        Scanner scanner = new Scanner(System.in);
            columns = scanner.nextInt();
            rows = scanner.nextInt();

        ArrayList<Integer[]> list = new ArrayList<>();
            for (int i = 0; i < rows; i++) {
                Integer[] temp = new Integer[columns];
                for (int j = 0; j < temp.length; j++) {
                    temp[j] = i + j;
                }
                list.add(temp);
            }

        for (Integer[] integers : list) {
            for (int i = 0; i < integers.length; i++) {
                System.out.print(integers[i] + " ");
            }
            System.out.println();
        }
    }
}
