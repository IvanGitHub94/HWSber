package HW3_3.Task4;

/*
4
Иван
Николай
Анна
Дарья
Жучка
Кнопка
Цезарь
Добряш
7 6 7
8 8 7
4 5 6
9 9 9
*/

import java.util.*;

public class UtilDogsEvent {
    public static int n;

    private UtilDogsEvent() {
    }
    public static List<Dog> makeDogsList() {
        Scanner scanner = new Scanner(System.in);
        n = scanner.nextInt();

        List<Participant> participants = new ArrayList<>();
        List<Dog> dogs = new ArrayList<>();
        List<Integer[]> matrix = new ArrayList<>();

        String temp;
        for (int i = 0; i < n; i++) {
            temp = scanner.next();
            participants.add(new Participant(temp));
        }

        for (int i = 0; i < n; i++) {
            temp = scanner.next();
            dogs.add(new Dog(temp, participants.get(i)));
        }

        ///////////////////////////////////////////////// Оценки
        for (int i = 0; i < n; i++) {
            Integer[] tempInt = new Integer[3];
            for (int j = 0; j < tempInt.length; j++) {
                tempInt[j] = scanner.nextInt();
            }
            matrix.add(tempInt);
        }

        double min = Double.MAX_VALUE;
        double sum = 0;
        double avg;
        double mul = Math.pow(10, 1);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 3; j++) {
                sum += matrix.get(i)[j];
            }
            avg = sum / 3;
            if(min > avg) {
                min = avg;
            }
            sum = 0;

            Dog dogTemp = dogs.get(i);
            dogTemp.setGradesAVG(((long)(avg * mul)) / mul); // сокращение кол-ва знаков после запятой
        }

        return dogs;
    }

    public static void main(String[] args) {
        List<Dog> dogs = makeDogsList();

        dogs.sort(new Dog());

        for (int i = 0; i < 3; i++) {
            System.out.println(dogs.get(i));
        }
    }
}
