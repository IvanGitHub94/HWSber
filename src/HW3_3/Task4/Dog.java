package HW3_3.Task4;

import java.util.Comparator;
import java.util.Objects;

public class Dog implements Comparator<Dog> {
    private String dogName;
    private double gradesAVG;

    private Participant participant;

    public Dog(String dogName, Participant participant) {
        try {
            if (dogName.length() > 0) {
                this.dogName = dogName;
            }
            else throw new NameLengthException();
        } catch (NameLengthException e) {
            e.getMessage();
        }
        this.participant = participant;
    }

    public Dog() {
    }

    public String getDogName() {
        return dogName;
    }

    public double getGradesAVG() {
        return gradesAVG;
    }

    public Participant getParticipant() {
        return participant;
    }

    public void setDogName(String dogName) {
        this.dogName = dogName;
    }

    public void setGradesAVG(double gradesAVG) {
        this.gradesAVG = gradesAVG;
    }

    public void setParticipant(Participant participant) {
        this.participant = participant;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return Double.compare(dog.gradesAVG, gradesAVG) == 0 && Objects.equals(dogName, dog.dogName) && Objects.equals(participant, dog.participant);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dogName, gradesAVG, participant);
    }

    @Override
    public String toString() {
        return dogName + ", средняя оценка: " + gradesAVG;
    }

    @Override
    public int compare(Dog o1, Dog o2) {
        return Double.compare(o2.getGradesAVG(), o1.getGradesAVG());
    }
}

class NameLengthException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение dogName.";
    }
}
