package HW3_3.Task4;

import java.util.Objects;

public class Participant {
    private String name;

    public Participant(String name) {
        try {
            if (name.length() > 0) {
                this.name = name;
            }
            else throw new ParticipantNameLengthException();
        } catch (ParticipantNameLengthException e) {
            e.getMessage();
        }
    }

    public Participant(){
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Participant that = (Participant) o;
        return Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Хозяин: " + name;
    }
}



class ParticipantNameLengthException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение name хозяина собаки.";
    }
}
