package HW3_3.Task1;

public class AnimalsTest {
    public static void main(String[] args) {
        System.out.println("Летучая мышь:");
        Bat bat = new Bat();
        bat.toEat();
        bat.toSleep();
        bat.wayOfBirth();
        bat.toFly();
        System.out.println("-------------------------------------");

        System.out.println("Дельфин:");
        Dolphin dolphin = new Dolphin();
        dolphin.toEat();
        dolphin.toSleep();
        dolphin.wayOfBirth();
        dolphin.toSwim();
        System.out.println("-------------------------------------");

        System.out.println("Золотая рыбка:");
        GoldFish goldFish = new GoldFish();
        goldFish.toEat();
        goldFish.toSleep();
        goldFish.wayOfBirth();
        goldFish.toSwim();
        System.out.println("-------------------------------------");

        System.out.println("Орел:");
        Eagle eagle = new Eagle();
        eagle.toEat();
        eagle.toSleep();
        eagle.wayOfBirth();
        eagle.toFly();
        System.out.println("-------------------------------------");

        System.out.println("Млекопитающие:");
        Mammal mammal = new Bat();
        mammal.toEat();
        mammal.toSleep();
        mammal.wayOfBirth();
        System.out.println("-------------------------------------");

        System.out.println("Рыбы:");
        Fish fish = new GoldFish();
        fish.toEat();
        fish.toSleep();
        fish.wayOfBirth();
        System.out.println("-------------------------------------");

        System.out.println("Птицы:");
        Bird bird = new Bird();
        bird.toEat();
        bird.toSleep();
        bird.wayOfBirth();
        System.out.println("-------------------------------------");
    }
}
