package HW3_3.Task1;

public abstract class Animal{
    void toEat() {
        System.out.println("This animal is eating.");
    }

    void toSleep() {
        System.out.println("This animal is sleeping.");
    }
}
