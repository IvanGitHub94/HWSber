package ProModule.HW1;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class FromValidator {

    public static void main(String[] args) {
        checkName("Qa");
        checkBirthdate("10/12/1900");
        checkGender("Female");
        checkHeight("156");
    }

    public static void checkName(String str) {
        int len = str.length();

        try {
            if (len < 2 || len >= 20) {
                throw new NameException("Некорректная длина имени.");
            }
            else if (!Character.isUpperCase(str.charAt(0))) {
                throw new NameException("Имя должно начинаться с заглавной буквы.");
            }
            else System.out.println("Успешный ввод (имя)!");
        }
        catch (NameException e) {
            e.printStackTrace();
        }
    }

    public static void checkBirthdate(String str) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate now = LocalDate.now();
        LocalDate thisString;
        LocalDate oldDate = LocalDate.of(1900, 1, 1);

        try {
            thisString = LocalDate.parse(str, formatter);

            if(thisString.isAfter(now)) {
                throw new BirthdateException("Дата не должна быть позже текущей.");
            }
            else if (thisString.isBefore(oldDate)) {
                throw new BirthdateException("Дата не должна быть ранее 01.01.1900 .");
            }
            else System.out.println("Успешный ввод (дата)!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void checkGender(String str) {
        try {
            if(!str.equalsIgnoreCase(String.valueOf(Gender.MALE)) &&
                    !str.equalsIgnoreCase(String.valueOf(Gender.FEMALE))) {
                throw new GenderException("Неизвестный гендер.");
            }
            else System.out.println("Успешный ввод (гендер)!");
        } catch (GenderException e) {
            e.printStackTrace();
        }
    }

    public static void checkHeight(String str) {
        try{
            double height = Double.parseDouble(str);

            if(height <= 0) {
                throw new HeightException("Рост не может быть меньше или равен 0.");
            }
            else System.out.println("Успешный ввод (рост)! " + height);
        }
        catch (NumberFormatException | HeightException e) {
            e.printStackTrace();
        }
    }
}

class NameException extends Exception {
    NameException (String message) {
        super(message);
    }
}
class BirthdateException extends Exception {
    BirthdateException (String message) {
        super(message);
    }
}
class GenderException extends Exception {
    GenderException (String message) {
        super(message);
    }
}
class HeightException extends Exception {
    HeightException (String message) {
        super(message);
    }
}

enum Gender {
    MALE,
    FEMALE
}