package ProModule.HW1;

public class Main {
    public static void main(String[] args)  {
        try {
            inputN();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        System.out.println("Успешный ввод!");
    }

    private static void inputN() throws Exception {
        System.out.println("Введите число n, 0 < n < 100");
        java.util.Scanner scanner = new java.util.Scanner(System.in);
            int n = scanner.nextInt();
                if (n >= 100 || n <= 0) {
                    throw new Exception("Неверный ввод");
                }
        }
}

