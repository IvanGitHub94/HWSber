package ProModule.HW1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class WriteToFile {
    private WriteToFile() {
    }

    public static void main(String[] args) {
        try {
            readFile();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void readFile() throws IOException {
        String s;
        try (BufferedReader br = new BufferedReader(new FileReader("src/ProModule/HW1/input.txt"))) {
            while ((s = br.readLine()) != null) {
                writeTo(s + "\n");
                System.out.println(s);
            }
        }
    }

    public static void writeTo(String s) throws IOException {
        Path path = Paths.get("src/ProModule/HW1/output.txt");

        Files.write(
                path,
                s.toUpperCase().getBytes(),
                StandardOpenOption.CREATE,
                StandardOpenOption.APPEND
        );
    }
}
