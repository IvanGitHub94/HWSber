package ProModule.HW1;

import java.util.Scanner;

/*

5
1 3 5 4 5

3
3 2 1

*/

//TODO: В ТГ-канале "Java 16 сб НОВОСТИ" было пояснение насчет данной задачи:
// "в дополнительной задаче номер 1 вам необходимо решить задачу за линейную сложность"

public class AdditionalTask1 {
    public static void main(String[] args) {
        int n, max, max2, count = 0;

        max = Integer.MIN_VALUE;

        Scanner scanner = new Scanner(System.in);

        n = scanner.nextInt();

            int[] arr = new int[n];
            int[] arr1 = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        for (int i = 0; i < n; i++) {
            if(arr[i] >= max) {
                max = arr[i];
            }
        }

        for (int i : arr) {
            if(i == max) {
                count++;
                if (count > 1) break;
            }
        }

            if(count > 1) {
                max2 = max;
            }
            else {
                max2 = Integer.MIN_VALUE;

                for (int i = 0; i < n; i++) {
                    if (arr[i] != max) {
                        arr1[i] = arr[i];
                    }
                }

                for (int i = 0; i < n; i++) {
                    if(arr1[i] >= max2) {
                        max2 = arr1[i];
                    }
                }
            }

        System.out.println(max + " " + max2);
    }
}
