package ProModule.HW1;

import java.util.Scanner;

/*

5
-42 -12 3 5 8
5

2
17 19
20

*/

public class AdditionalTask2 {
    public static void main(String[] args) {
        int n, p;
        Scanner scanner = new Scanner(System.in);

        n = scanner.nextInt();

        int[] arr = new int[n];

        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }

        p = scanner.nextInt();

        System.out.println(logNSearch(arr, p));
    }

    static int logNSearch(int[] arr, int key) {
        int low = 0;
        int high = arr.length - 1;

        int res;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int midVal = arr[mid];

            if (midVal < key)
                low = mid + 1;
            else if (midVal > key)
                high = mid - 1;
            else
                return mid;
        }
        res = -(low + 1);

        if(res < 0) {
            res = -1;
        }
        return res;
    }
}
