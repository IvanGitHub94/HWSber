package ProModule.HW1;

public class MyEvenNumber {
    int n;

    public MyEvenNumber(int n) throws MyEvenException {
            if(n % 2 == 0) {
                this.n = n;
            } else throw new MyEvenException();
    }
}
class MyEvenException extends Exception {
}

// Тестирование создания объекта
class TestEvenNumber {
    public static void main(String[] args) {
        MyEvenNumber number1;
        try {
            number1 = new MyEvenNumber(2);
        } catch (MyEvenException e) {
            throw new RuntimeException(e);
        }

        System.out.println(number1);
    }
}
