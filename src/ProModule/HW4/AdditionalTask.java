package ProModule.HW4;

import java.util.Arrays;

public class AdditionalTask {
    public static void main(String[] args) {
        String str1 = "cat";
        String str2 = "cats";
            System.out.println(copareStrings(str1, str2));

        str1 = "cat";
        str2 = "cut";
            System.out.println(copareStrings(str1, str2));

        str1 = "cat";
        str2 = "nut";
            System.out.println(copareStrings(str1, str2));
    }

    public static boolean copareStrings(String str1, String str2) {

        Character[] characters1 = new Character[str1.length()];
        Character[] characters2 = new Character[str2.length()];

            for (int i = 0; i < str1.length(); i++) {
                characters1[i] = str1.toCharArray()[i];
            }
            for (int i = 0; i < str2.length(); i++) {
                characters2[i] = str2.toCharArray()[i];
            }

        Character[] characters3 = Arrays.stream(characters1)
                .filter(ch1 -> Arrays.asList(characters2).contains(ch1))
                .toArray(Character[]::new);

        return characters3.length > 1;
    }
}
