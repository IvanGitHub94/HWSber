package ProModule.HW4;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Task6 {
    public static void main(String[] args) {
        Set<Integer> set1 = new HashSet<>(Arrays.asList(1, 2, 5, 90, 40));
        Set<Integer> set2 = new HashSet<>(Arrays.asList(6, 54, 3, 9, 455));
        Set<Integer> set3 = new HashSet<>(Arrays.asList(4, 4, 12, -7, 900));
        Set<Integer> set4 = new HashSet<>(Arrays.asList(543, -100, -31, 0, 98));

        Set<Set<Integer>> sets = new HashSet<>();
            sets.add(set1);
            sets.add(set2);
            sets.add(set3);
            sets.add(set4);

        System.out.println(transform(sets));
    }

    public static Set<Integer> transform(Set<Set<Integer>> s) {
        Set<Set<Integer>> sets = new HashSet<>(s);
        Set<Integer> res = sets.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());

        return res;
    }
}
