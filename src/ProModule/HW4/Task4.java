package ProModule.HW4;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Task4 {
    public static void main(String[] args) {
        List<Integer> l = new ArrayList<>();
            l.add(2);
            l.add(0);
            l.add(-56);
            l.add(700);
            l.add(-1);
            l.add(43);
            l.add(1);

        System.out.println(sortReverse(l));
        System.out.println(l);
    }

    public static List<Integer> sortReverse(List<Integer> integers) {

        List<Integer> list = new ArrayList<>(integers);

        return list.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());
    }
}
