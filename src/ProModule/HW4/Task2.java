package ProModule.HW4;

import java.util.List;

public class Task2 {
    public static void main(String[] args) {
        List<Integer> test = List.of(1, 2, 3, 4, 5);

        System.out.println(multipleOfList(test));
    }

    public static Integer multipleOfList(List<Integer> list) {

        return list.stream()
                .reduce((i1, i2) -> i1 * i2).get();
    }
}
