package ProModule.HW4;

import java.util.stream.IntStream;

public class Task1 {
    public static void main(String[] args) {

        Integer i = IntStream.range(1, 101) // 101 - так как правая граница диапазона не включается
                .filter(x -> x%2==0)
                .sum();

        System.out.println(i);
    }
}
