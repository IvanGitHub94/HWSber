package ProModule.HW4;

import java.util.List;
import java.util.stream.Collectors;

public class Task5 {
    public static void main(String[] args) {
        List<String> test = List.of("abc", "def", "qqq");

        printUpperCase(test);
    }

    public static void printUpperCase(List<String> list) {

        System.out.println(list.stream()
                .map(String::toUpperCase)
                .collect(Collectors.joining(", "))
        );
    }
}
