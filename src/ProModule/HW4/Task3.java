package ProModule.HW4;

import java.util.List;

public class Task3 {
    public static void main(String[] args) {
        List<String> test = List.of("abc", "", "", "def", "qqq");

        System.out.println(countOfEmpty(test));
    }

    public static long countOfEmpty(List<String> list) {

        return list.stream()
                .filter(s -> !s.equals(""))
                .count();
    }
}
