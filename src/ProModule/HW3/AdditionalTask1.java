package ProModule.HW3;

public class AdditionalTask1 {
    public static void main(String[] args) {
        System.out.println(isRight("(()())"));
    }

    public static boolean isRight(String str) {
        String s = "()";

        if (str.length() % 2 != 0) {
            return false;
        }

        if(!str.contains(s) || !str.startsWith("(") || !str.endsWith(")")) {
            return false;
        }
        else {
                int len = str.length();
                String a = str.substring(0, len / 2);
                String b = str.substring(len / 2);

                StringBuilder sb = new StringBuilder();
                for (char c : b.toCharArray()) {
                    if(c == '(') {
                        sb.append(')');
                    } else if (c == ')') {
                        sb.append('(');
                    }
                }

                    String z = sb.toString();

                    StringBuilder sb2 = new StringBuilder();
                    for (int i = z.length() - 1; i >= 0 ; i--) {
                        sb2.append(z.charAt(i));
                    }

                return a.equals(sb2.toString());
            }
    }
}
