package ProModule.HW3;

import javax.management.InvalidApplicationException;

public class APrinter {
    public void print(int a) throws InvalidApplicationException, ArithmeticException {
        // "throws InvalidApplicationException, ArithmeticException" - добавлено для примера,
        // чтобы проверить вывод массива исключений этого метода
        System.out.println(a / 0);
    }
}
