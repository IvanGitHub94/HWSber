package ProModule.HW3;

import java.util.Arrays;

public class Task4 implements MyInterfaceTwo{
    public static void main(String[] args) {
        printInterfacesOfClass("ProModule.HW3.Task4Test");
    }

    public static void printInterfacesOfClass(String c) {
        try {
            Class cl = Class.forName(c);
            System.out.println("Интерфейсы класса: ");
            System.out.println(Arrays.toString(cl.getInterfaces()));

            System.out.println();
            System.out.println("Интерфейсы класса-родителя: ");
            System.out.println(Arrays.toString(cl.getSuperclass().getInterfaces()));

            System.out.println();
            System.out.println("Родители интерфейсов класса: ");
            Class[] interfaces = cl.getInterfaces();
            for (Class cls : interfaces) {
                Class[] parents = cls.getInterfaces();
                for (Class clsParent : parents) {
                    if (clsParent != null) {
                        System.out.println(clsParent.toString());
                    }
                }
            }

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
