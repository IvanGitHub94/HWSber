package ProModule.HW3;

import java.lang.annotation.Annotation;

public class Task2 {
    public static void main(String[] args) {
        checkAnnotation("ProModule.HW3.TestTask1");
    }

    public static void checkAnnotation(String c) {
        try {
            Class cl = Class.forName(c);
            if (!cl.isAnnotationPresent(Task1MyAnnotation.isLike.class)) {
                System.out.println("Аннотации отсутствуют.");
            }
            else {
                System.out.println("Класс имеет аннотацию " + cl.getAnnotation(Task1MyAnnotation.isLike.class));

                Annotation annotation = cl.getAnnotation(Task1MyAnnotation.isLike.class);
                Task1MyAnnotation.isLike annotationRes = (Task1MyAnnotation.isLike) annotation;

                System.out.println("Значение поля аннотации: " + annotationRes.b());
            }
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
}
