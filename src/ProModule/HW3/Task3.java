package ProModule.HW3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class Task3 {
    public static void main(String[] args) {
        reflectionCallMethod();
    }

    public static void reflectionCallMethod() {
        Class c;
        APrinter aPrinter;
        try {

            aPrinter = new APrinter();

            c = Class.forName(APrinter.class.getName());

            Method printMethod = c.getDeclaredMethod("print", int.class);

            printMethod.setAccessible(true);
                // на случай, если будет передаваться не public метод,
                // чтобы сэкономить время на проверке доступа

            Class<?>[] exceptionTypes = printMethod.getExceptionTypes();
            System.out.println(Arrays.toString(exceptionTypes));
                // вывод массива исключений,
                // которые могут быть выброшены вызываемым методом

            printMethod.invoke(aPrinter, 8);

        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Класс не найден.");
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Метод с такой сигнатурой не найден.");
        } catch (IllegalArgumentException e) {
            throw new RuntimeException("Неверное значение аргумента (аргументов).");
        } catch (InvocationTargetException e) {
            throw new RuntimeException("Исключение выброшено вызываемым методом.");
        } catch (IllegalAccessException e) {
            throw new RuntimeException("Не удалось получить доступ к целевому методу.");
        }
    }
}
