package ProModule.HW3;

public class AdditionalTask2 {
    public static void main(String[] args) {
        System.out.println(isRightSequence("[{(){}}][()]{}"));
    }

    public static boolean isRightSequence(String str)
    {
        int length1;
        int length2;
        String s = str.replaceAll("[^()\\[\\]{}]","");
        while(true)
        {
            length1 = s.length();
            s = s.replaceAll("\\(\\)","");
            s = s.replaceAll("\\[\\]","");
            s = s.replaceAll("\\{\\}","");
            length2 = s.length();

            if (length1 == length2) break;
        }
        return (length2 == 0);
    }
}
