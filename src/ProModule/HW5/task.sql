create table public.flowers (
    id serial primary key,
    title varchar(50) unique check (title <> '') not null,
    cost integer not null
);

insert into public.flowers (title, cost) values ('Розы', 100);
insert into public.flowers (title, cost) values ('Лилии', 50);
insert into public.flowers (title, cost) values ('Ромашки', 25);

create table public.customers (
    id serial primary key,
    name varchar(50) not null,
    phone varchar(12) not null
);

insert into public.customers (name, phone) values ('Иван', 89991234567);
insert into public.customers (name, phone) values ('Евгений', 89997654321);
insert into public.customers (name, phone) values ('Марина', 89991111111);
insert into public.customers (name, phone) values ('Ольга', 89990000000);
insert into public.customers (name, phone) values ('Петр', 89992222222);

create table if not exists public.buy (
    id serial primary key,
    customer_id integer references public.customers (id) not null,
    flower_id integer references public.flowers (id) not null,
    count integer check (count > 0 and count < 1001) not null,
    date timestamp not null
);

insert into public.buy (customer_id, flower_id, count, date)
values (1, 1, 15, '2023-03-07'),
(1, 2, 11, '2023-03-07'),
(2, 1, 7, '2023-03-08'),
(2, 2, 9, '2023-03-08'),
(2, 3, 9, '2023-03-08'),
(3, 1, 5, '2023-03-07'),
(4, 1, 17, '2023-03-08'),
(4, 2, 9, '2023-03-08'),
(5, 1, 33, '2023-03-07'),
(5, 3, 19, '2023-03-08');

--1
select b.id, f.title, b.count, c.name, c.phone
from public.buy b
inner join public.customers c on c.id = b.customer_id
inner join public.flowers f on f.id = b.flower_id
where b.id = 3;

--2
select b.id, f.title, b.count, b.date, c.name
from public.buy b
inner join public.customers c on c.id = b.customer_id
inner join public.flowers f on f.id = b.flower_id
where c.id = 5;

--3
select b.id, b.date, f.title, b.count
from public.buy b
inner join public.flowers f on f.id = b.flower_id
order by count desc limit 1;

--4
select sum(f.cost * b.count) as "Выручка"
from public.buy b
inner join public.flowers f on f.id = b.flower_id;