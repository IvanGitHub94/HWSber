package ProModule.HW2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;

public class Task1 {
    private Task1() {
    }

    public static void main(String[] args) {
        // ------------------------- Код для тестирования -------------------------
        List<Person> baseList = new ArrayList<>();
            baseList.add(new Person("Anna", 31, Gender.FEMALE));
            baseList.add(new Person("Anna", 31, Gender.FEMALE));

            baseList.add(new Person("Andrey", 45, Gender.MALE));
            baseList.add(new Person("Andrey", 45, Gender.MALE));

            baseList.add(new Person("Petr", 26, Gender.MALE));
            baseList.add(new Person("Ivan", 22, Gender.MALE));
            baseList.add(new Person("Irina", 19, Gender.FEMALE));
            baseList.add(new Person("Marina", 20, Gender.FEMALE));

        List<Integer> ints = new ArrayList<>();
            ints.add(1);
            ints.add(1);

            ints.add(2);
            ints.add(3);
            ints.add(4);

            ints.add(5);
            ints.add(5);
            ints.add(5);

        HashSet<Person> res = unique(baseList);
        HashSet<Integer> res2 = unique(ints);

        for (Person p : res) {
            System.out.println(p);
        }

        System.out.println("\n" + res2);
    }

    public static <T> HashSet<T> unique(List<T> baseList) {
        return new HashSet<>(baseList);
    }
}

class Person {
    // ------------------------- Класс для тестирования -------------------------
    String name;
    int age;
    Gender gender;

    public Person(String name, int age, Gender gender) {
        this.name = name;
        this.age = age;
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", gender=" + gender +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return age == person.age && Objects.equals(name, person.name) && gender == person.gender;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, gender);
    }
}

enum Gender {
    MALE,
    FEMALE
}
