package ProModule.HW2;

import java.util.*;

public class Task4 {
    private Task4() {
    }

    public static void main(String[] args) {
        List<Document> docs = new ArrayList<>();
            docs.add(new Document(1, "file_1", 10));
            docs.add(new Document(2, "file_2", 51));
            docs.add(new Document(3, "file_3", 7));
            docs.add(new Document(4, "file_4", 11));
            docs.add(new Document(5, "file_5", 10));
            docs.add(new Document(6, "file_6", 3));

        finder(56, docs);
        finder(4, docs);
        finder(0, docs);
        finder(1, docs);
    }

    public static void finder(int id, List<Document> documents) {
        Optional<Document> optionalDocument = searchDoc(id, documents);
        if (optionalDocument.isPresent()) {
            System.out.println(optionalDocument.get());
        }
        else System.out.println("Документ с id " + id + " не найден.");
    }

    private static Optional<Document> searchDoc(int id, List<Document> documents) {
        Map<Integer, Document> docs = organizeDocuments(documents);

        Document result = docs.get(id);
        return Optional.ofNullable(result);
    }

    private static Map<Integer, Document> organizeDocuments(List<Document> documents) {
        HashMap<Integer, Document> res = new HashMap<>();

        for (Document document : documents) {
            res.put(document.getId(), document);
        }
        return res;
    }
}

class Document {
    public int id;
    public String name;
    public int pageCount;

    public Document(int id, String name, int pageCount) {
        this.id = id;
        this.name = name;
        this.pageCount = pageCount;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", pageCount=" + pageCount +
                '}';
    }
}

