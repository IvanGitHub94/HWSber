package ProModule.HW2;

import java.util.HashSet;
import java.util.Set;

public class Task3 {
}

class PowerfulSet {
// ------------------------- Код для тестирования (пункт a) -------------------------
    public static void main(String[] args) {
        Set<Integer> tSet1A = new HashSet<>();
            tSet1A.add(1);
            tSet1A.add(2);
            tSet1A.add(3);
        Set<Integer> tSet2A = new HashSet<>();
            tSet2A.add(0);
            tSet2A.add(1);
            tSet2A.add(2);
            tSet2A.add(4);

        System.out.println(intersection(tSet1A, tSet2A));

// ------------------------- Дополнительный тест (пункт a) -------------------------
        Set<Person> pSet1A = new HashSet<>();
            pSet1A.add(new Person("Anna", 31, Gender.FEMALE));
            pSet1A.add(new Person("Andrey", 45, Gender.MALE));
            pSet1A.add(new Person("Petr", 26, Gender.MALE));
            pSet1A.add(new Person("Ivan", 22, Gender.MALE));
        Set<Person> pSet2A = new HashSet<>();
            pSet2A.add(new Person("Anna", 31, Gender.FEMALE));
            pSet2A.add(new Person("Andrey", 45, Gender.MALE));
            pSet2A.add(new Person("Irina", 19, Gender.FEMALE));
            pSet2A.add(new Person("Marina", 20, Gender.FEMALE));

        System.out.println(intersection(pSet1A, pSet2A));

        System.out.println();
        System.out.println("========================================================================");

// ------------------------- Код для тестирования (пункт b) -------------------------
        Set<Integer> tSetB = new HashSet<>();
            tSetB.add(1);
            tSetB.add(2);
            tSetB.add(3);
        Set<Integer> tSet1B = new HashSet<>();
            tSet1B.add(0);
            tSet1B.add(1);
            tSet1B.add(2);
            tSet1B.add(4);

        System.out.println(union(tSetB, tSet1B));

// ------------------------- Дополнительный тест (пункт b) -------------------------
        Set<Person> pSet1B = new HashSet<>();
            pSet1B.add(new Person("Anna", 31, Gender.FEMALE));
            pSet1B.add(new Person("Andrey", 45, Gender.MALE));
            pSet1B.add(new Person("Petr", 26, Gender.MALE));
            pSet1B.add(new Person("Ivan", 22, Gender.MALE));
        Set<Person> pSet2B = new HashSet<>();
            pSet2B.add(new Person("Anna", 31, Gender.FEMALE));
            pSet2B.add(new Person("Andrey", 45, Gender.MALE));
            pSet2B.add(new Person("Irina", 19, Gender.FEMALE));
            pSet2B.add(new Person("Marina", 20, Gender.FEMALE));

        // Цикл для удобства отображения
        Set<Person> pSetRes = union(pSet1B, pSet2B);
            for (Person p : pSetRes) {
                System.out.println(p);
            }

        System.out.println();
        System.out.println("========================================================================");

// ------------------------- Код для тестирования (пункт c) -------------------------
        Set<Integer> tSet1C = new HashSet<>();
            tSet1C.add(1);
            tSet1C.add(2);
            tSet1C.add(3);
        Set<Integer> tSet2C = new HashSet<>();
            tSet2C.add(0);
            tSet2C.add(1);
            tSet2C.add(2);
            tSet2C.add(4);

        System.out.println(relativeComplement(tSet1C, tSet2C));
        //System.out.println(tSet1C);

// ------------------------- Дополнительный тест (пункт b) -------------------------
        Set<Person> pSet1C = new HashSet<>();
            pSet1C.add(new Person("Anna", 31, Gender.FEMALE));
            pSet1C.add(new Person("Andrey", 45, Gender.MALE));
            pSet1C.add(new Person("Petr", 26, Gender.MALE));
            pSet1C.add(new Person("Ivan", 22, Gender.MALE));
        Set<Person> pSet2C = new HashSet<>();
            pSet2C.add(new Person("Anna", 31, Gender.FEMALE));
            pSet2C.add(new Person("Andrey", 45, Gender.MALE));
            pSet2C.add(new Person("Irina", 19, Gender.FEMALE));
            pSet2C.add(new Person("Marina", 20, Gender.FEMALE));

        System.out.println(relativeComplement(pSet1C, pSet2C));
    }

// ------------------------- Задание a -------------------------
    public static <T> Set<T> intersection(Set<T> set1, Set<T> set2) {
        Set<T> res = new HashSet<>();

        for (T elem1: set1) {
            for (T elem2 : set2) {
                if (elem1.equals(elem2)) {
                    res.add(elem1);
                }
            }
        }
        return res;
    }

// ------------------------- Задание b -------------------------
    public static <T> Set<T> union(Set<T> set1, Set<T> set2) {
        Set<T> res = new HashSet<>();
            res.addAll(set1);
            res.addAll(set2);
        return res;
    }

// ------------------------- Задание c -------------------------
    public static <T> Set<T> relativeComplement(Set<T> set1, Set<T> set2) {
        // Работа именно с копией первого набора, чтобы он не изменялся
        Set<T> copy = new HashSet<>();
        copy.addAll(set1);

        copy.removeAll(set2);

        return copy;
    }
}
