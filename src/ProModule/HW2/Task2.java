package ProModule.HW2;

import java.util.*;

public class Task2 {
    public static void main(String[] args) {
        System.out.println(isAnagram());
    }

    public static boolean isAnagram() {
        String s, t;
        Scanner scan = new Scanner(System.in);
            s = scan.nextLine();
            t = scan.nextLine();

        List<Character> list1 = new ArrayList<>();
        for (char c : s.toCharArray()) {
            list1.add(c);
        }

        List<Character> list2 = new ArrayList<>();
        for (char c : t.toCharArray()) {
            list2.add(c);
        }

        scan.close();

            if (list1.size() == 0 || list2.size() == 0) {
                return false;
            }
            else if (list1.size() != list2.size()) {
                return false;
            }

        list1 = new ArrayList<>(list1);
        list2 = new ArrayList<>(list2);

        Collections.sort(list1);
        Collections.sort(list2);

        return list1.equals(list2);
    }
}
