package ProModule.HW2;

import java.util.*;

public class AdditionalTask {
    public static void main(String[] args) {
        String[] words = new String[]
                {"the","day","is","sunny","the","the","the","sunny","is","is","day"};

        String[] res = frequentWords(words, 4);

        System.out.println(Arrays.toString(res));
    }

    public static String[] frequentWords(String[] arr, int k) {
        HashMap<String, Integer> wordsMap = new HashMap<>();
        HashSet<String> uniqueWordsTemp = new HashSet<>(Arrays.asList(arr));

        int count;
        for (String s : uniqueWordsTemp) {
            count = 0;
            for (String s1 : arr) {
                if(s.equalsIgnoreCase(s1)) {
                    count++;
                }
            }
            wordsMap.put(s, count);
        }

        List<Map.Entry<String, Integer>> entryList = new ArrayList<>();
        for (Map.Entry<String, Integer> entry : wordsMap.entrySet()) {
            entryList.add(entry);
        }

        Collections.sort(entryList, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return 0;
            }
        });

        int loopsCount = entryList.size();

            if (k <= loopsCount) {
                loopsCount = k;
            }

        String[] result = new String[loopsCount];

        for (int i = 0; i < loopsCount; i++) {
            result[i] = entryList.get(i).getKey();
        }

        return result;
    }
}
