package HW3_1;

public class TimeUnit {

    public static void main(String[] args) {
        TimeUnit timeUnit = new TimeUnit(3, 0);
        System.out.println(timeUnit);

        System.out.println("-----------------------------------------");
        timeUnit.addTime(5, 40, 12345);

        System.out.println(timeUnit.toStringAmPm());
    }
    private String hours;
    private String minutes;
    private String seconds;

    public TimeUnit(int hours, int minutes, int seconds) {
        this.setHours(hours);
        this.setMinutes(minutes);
        this.setSeconds(seconds);
    }

    public TimeUnit(int hours, int minutes) {
        this.setHours(hours);
        this.setMinutes(minutes);
        this.setSeconds(0);
    }

    public TimeUnit(int hours) {
        this.setHours(hours);
        this.setMinutes(0);
        this.setSeconds(0);
    }

    public TimeUnit() {
        this.hours = "00";
        this.minutes = "00";
        this.seconds = "00";
    }

    public void setHours(int hours) {
        if (hours >= 0 && hours <= 23) {
            if (hours <= 9) this.hours = "0" + hours;
            else this.hours = String.valueOf(hours);
        }
        else this.hours = "00";
    }

    public void setMinutes(int minutes) {
        if (minutes >= 0 && minutes <= 59) {
            if (minutes <= 9) this.minutes = "0" + minutes;
            else this.minutes = String.valueOf(minutes);
        }
        else this.minutes = "00";
    }

    public void setSeconds(int seconds) {
        if (seconds >= 0 && seconds <= 59) {
            if (seconds <= 9) this.seconds = "0" + seconds;
            else this.seconds = String.valueOf(seconds);
        }
        else this.seconds = "00";
    }

    public String getHours() {
        return hours;
    }

    public String getMinutes() {
        return minutes;
    }

    public String getSeconds() {
        return seconds;
    }

    @Override
    public String toString() {
        return hours + ":" + minutes + ":" + seconds;
    }

    public String toStringAmPm() {
        int h = Integer.parseInt(this.getHours());

        int hPm = h - 12;

        if(h == 12) {
            return h + ":" + minutes + ":" + seconds + " pm";
        }
        else if (h > 12) {
            if (hPm <= 9) {
                return "0" + hPm + ":" + minutes + ":" + seconds + " pm";
            }
            else return hPm + ":" + minutes + ":" + seconds + " pm";
        }
        else {
            if(h <= 9) {
                return "0" + h + ":" + minutes + ":" + seconds + " am";
            }
            else return h + ":" + minutes + ":" + seconds + " am";
        }
    }

    public void addTime(int hours, int minutes, int seconds) {
        int h = Integer.parseInt(this.getHours());
        int m = Integer.parseInt(this.getMinutes());
        int s = Integer.parseInt(this.getSeconds());

        int plusH = h + hours;
        int plusM = m + minutes;
        int plusS = s + seconds;

        int hoursR_1 = plusS / 60 / 60;
        int minutesR_1 = plusS / 60 % 60;
        int secondsR_1 = plusS % 60;


        setSeconds(secondsR_1);
        setMinutes((plusM + minutesR_1) % 60);
        setHours((plusH + hoursR_1) % 24 + (plusM + minutesR_1) / 60);
    }
}
