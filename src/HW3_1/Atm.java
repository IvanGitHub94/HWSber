package HW3_1;

public class Atm {
    private static int atmCount = 0;
    private double roublesCourse;
    private double dollarsCourse;

    public Atm(double roublesCourse, double dollarsCourse) {
        if(roublesCourse > 0 && dollarsCourse > 0) {
            this.roublesCourse = roublesCourse;
            this.dollarsCourse = dollarsCourse;
        }
        else {
            // курсы обмена по умолчанию
            this.roublesCourse = 73.2;
            this.dollarsCourse = 0.014;
        }
        atmCount++;
    }

    public double roublesToDollar(double sum) {
        return sum / roublesCourse;
    }

    public double dollarsToRoubles(double sum) {
        return sum / dollarsCourse;
    }

    public static void main(String[] args) {
        Atm atm = new Atm(70.2, 0.014);

        double sum1 = 178;
        double sum2 = 100;
        System.out.println("Количество долларов в сумме (" + sum1 + ") руб. : " + atm.roublesToDollar(sum1));
        System.out.println("Количество рублей в сумме (" + sum2 + ") долл. : " + atm.dollarsToRoubles(sum2));
        System.out.println("Количество банкоматов: " + atmCount);

        System.out.println();

        Atm atm1 = new Atm(68.7, 0.01455);

        sum1 = 189;
        sum2 = 101;
        System.out.println("Количество долларов в сумме (" + sum1 + ") руб. : " + atm1.roublesToDollar(sum1));
        System.out.println("Количество рублей в сумме (" + sum2 + ") долл. : " + atm1.dollarsToRoubles(sum2));
        System.out.println("Количество банкоматов: " + atmCount);
    }
}
