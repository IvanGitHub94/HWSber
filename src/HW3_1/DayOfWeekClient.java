package HW3_1;

public class DayOfWeekClient {
    public static void main(String[] args) {
        byte b = 1;

        DayOfWeek[] days = new DayOfWeek[7];

        for (int i = 0; i < days.length; i++) {
            days[i] = new DayOfWeek(b);
            b++;
        }

        for (DayOfWeek d : days) {
            System.out.println(d);
        }
    }
}
