package HW3_1;

import java.util.Arrays;

public class AmazingString {

    public static void main(String[] args) {
        AmazingString amazingString = new AmazingString(" Wow!");
        char[] a = new char[]{'a', '!'};

        // 1
        System.out.println(amazingString.symbolOf(3));

        // 2
        System.out.println(amazingString.stringLength());

        // 3
        amazingString.printStr();

        // 4
        System.out.println(amazingString.checkSubstring(a));
        System.out.println();

        // 5
        System.out.println(amazingString.checkSubstring("w!"));
        System.out.println();

        // 6
        // До удаления пробелов
        System.out.println(Arrays.toString(amazingString.chArr));

        amazingString.deleteForwardSpaces();

        // После
        System.out.println(Arrays.toString(amazingString.chArr));

        // 7
        amazingString.reverse();
        System.out.println(Arrays.toString(amazingString.chArr));
    }
    private char[] chArr;

    public AmazingString(char[] chArr) {
        this.chArr = chArr;
    }

    public AmazingString(String s) {
        this.chArr = s.toCharArray();
    }

    public char[] getChArr() {
        return chArr;
    }

    public void setChArr(char[] chArr) {
        this.chArr = chArr;
    }

    public char symbolOf(int i) {

        if(i >= 0 && i < this.chArr.length) {
            return this.chArr[i];
        }
        else return '\u0000';
    }

    public int stringLength() {
        return this.chArr.length;
    }

    public void printStr() {
        for (char c : this.chArr) {
            System.out.print(c);
        }
        System.out.println();
    }

    public boolean checkSubstring(String s) {
        return checkSubstring(s.toCharArray());
    }
    public boolean checkSubstring(char[] chars) {
        boolean b = false;
        int temp;

        if(chars.length == 0) return false;

        if (chars.length > this.chArr.length) return false;
        else {
            if (chars.length > 1) {
                for (int i = 1; i < this.chArr.length; i++) {
                    if(chars[0] == this.chArr[i - 1]) {
                        temp = i;
                        for (int j = 1; j < chars.length; j++, temp++) {
                            if(chars[j] != this.chArr[temp]) {
                                b = false;
                                break;
                            }
                            else {
                                b = true;
                            }
                        }
                    }
                }
            }
            else {
                for (char c : this.chArr) {
                    if (chars[0] == c) {
                        b = true;
                        break;
                    }
                }
            }
        }
        return b;
    }

    public void deleteForwardSpaces() {
        int count = 0;
        for (int i = 0; i < this.chArr.length; i++) {
            if (this.chArr[i] != ' ') {
                break;
            }
            else count++;
        }

        if (count != 0) {
            char[] withoutSpaces = new char[this.chArr.length - count];

            for (int i = count, j = 0; i < this.chArr.length; i++, j++) {
                withoutSpaces[j] = this.chArr[i];
            }

            this.setChArr(withoutSpaces);
        }
    }

    public void reverse() {
        int size = this.chArr.length;
        char temp;
        for (int i = 0; i < size / 2; i++) {
            temp = this.chArr[i];
            this.chArr[i] = this.chArr[size - 1 - i];
            this.chArr[size - 1 - i] = temp;
        }
    }
}
