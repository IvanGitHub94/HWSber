package HW3_1;

public class Cat {

    public static void main(String[] args) {
        Cat cat = new Cat();
        cat.status();
    }

    public void status() {
        int random = (int) (Math.random() * 3);

        switch (random) {
            case 0 -> this.sleep();
            case 1 -> this.meow();
            case 2 -> this.eat();
        }
    }

    private void sleep() {
        System.out.println("Sleep");
    }

    private void meow() {
        System.out.println("Meow");
    }

    private void eat() {
        System.out.println("Eat");
    }
}
