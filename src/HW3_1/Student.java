package HW3_1;

import java.util.Arrays;

public class Student {
    private String name;
    private String surname;
    private int[] grades = new int[10];

    public Student() {
    }

    public Student(String name, String surname){
        if(name.length() < 1 || surname.length() < 1) {
            System.out.println("Имя или фамилия студента не должны быть короче 1 символа.");
            this.name = "Name";
            this.surname = "Surname";
        }
        else {
            this.name = name;
            this.surname = surname;
        }
    }

    public static void main(String[] args) {
        Student student = new Student("Ivan", "Petrov");
            int[] numbers = new int[]{3, 4, 5, 4, 5, 3, 3, 4, 5, 3};

        Student student2 = new Student("Viktor", "Simonov");
            int[] numbers2 = new int[]{3, 4, 5, 4, 5, 3};

        student.setGrades(numbers);
        student2.setGrades(numbers2);

        // Проверка
        // Добавление оценки в конец - вызов метода addGrade
        System.out.println(Arrays.toString(student.grades));
        System.out.println("---------------------------------1");
            student.addGrade(8);
        System.out.println(Arrays.toString(student.grades));
        System.out.println("---------------------------------1");
            student.addGrade(90);
        System.out.println(Arrays.toString(student.grades));

        System.out.println();

        System.out.println(Arrays.toString(student2.grades));
        System.out.println("---------------------------------2");
            student2.addGrade(17);
        System.out.println(Arrays.toString(student2.grades));
        System.out.println("---------------------------------2");
            student2.addGrade(61);
        System.out.println(Arrays.toString(student2.grades));

        System.out.println();
        // Среднее арифметическое - вызов метода gradesAVG
        System.out.println(student.gradesAVG());
        System.out.println(student2.gradesAVG());
    }

    public void addGrade(int grade) {
        for (int i = 1; i < grades.length; i++) {
            grades[i - 1] = grades[i];
        }
        grades[grades.length - 1] = grade;
    }

    public double gradesAVG() {
        int sum = 0;
        for (int grade : grades) {
            sum += grade;
        }
        return ((double) sum) / grades.length;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGrades(int[] grades) {
        if (grades.length <= 10) {
            this.grades = grades;
        }
        else {
            System.out.println("Размер массива должен быть не больше 10.");
        }
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int[] getGrades() {
        return grades;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                '}';
    }
}
