package HW3_1;

public final class DayOfWeek {
    byte dayNumber;
    String dayName;

    public DayOfWeek(byte dayNumber) {
        setDayNumber(dayNumber);
    }

    public DayOfWeek(String dayName) {
        setDayName(dayName);
    }

    public DayOfWeek() {
        setDayNumber((byte) ((int)(1 + Math.random() * 7)));
    }

    public byte getDayNumber() {
        return dayNumber;
    }

    public String getDayName() {
        return dayName;
    }

    public void setDayNumber(byte dayNumber) {
        if (dayNumber >= 1 && dayNumber <= 7) {
            this.dayNumber = dayNumber;
        }
        else {
            this.dayNumber = (byte) ((int)(1 + Math.random() * 7));
        }

        switch (this.dayNumber) {
            case 1 -> this.dayName = "Monday";
            case 2 -> this.dayName = "Tuesday";
            case 3 -> this.dayName = "Wednesday";
            case 4 -> this.dayName = "Thursday";
            case 5 -> this.dayName = "Friday";
            case 6 -> this.dayName = "Saturday";
            case 7 -> this.dayName = "Sunday";
        }
    }

    public void setDayName(String dayName) {
        switch (dayName) {
            case "Monday" -> {
                this.dayNumber = 1;
                this.dayName = "Monday";
            }
            case "Tuesday" -> {
                this.dayNumber = 2;
                this.dayName = "Tuesday";
            }
            case "Wednesday" -> {
                this.dayNumber = 3;
                this.dayName = "Wednesday";
            }
            case "Thursday" -> {
                this.dayNumber = 4;
                this.dayName = "Thursday";
            }
            case "Friday" -> {
                this.dayNumber = 5;
                this.dayName = "Friday";
            }
            case "Saturday" -> {
                this.dayNumber = 6;
                this.dayName = "Saturday";
            }
            case "Sunday" -> {
                this.dayNumber = 7;
                this.dayName = "Sunday";
            }
            default -> setDayNumber( (byte) ((int)(1 + Math.random() * 7)) );
        }
    }

    @Override
    public String toString() {
        return dayNumber + " " + dayName;
    }
}
