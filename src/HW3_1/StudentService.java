package HW3_1;

import java.util.Arrays;

public class StudentService {

    private static Student[] students = new Student[]{};

    public Student[] getStudents() {
        return students;
    }

    public static void main(String[] args) {
        Student student = new Student("Ivan", "Antonov");
        int[] numbers = new int[]{3, 4, 5, 4, 5, 3, 3, 4, 5, 3};

            student.setGrades(numbers);

        Student student2 = new Student("Igor", "Climovskiy");
        int[] numbers2 = new int[]{3, 4, 5, 4, 5, 3};

            student2.setGrades(numbers2);

        Student student3 = new Student("Stanislav", "Dobrynin");
        int[] numbers3 = new int[]{3, 3, 3, 4, 3, 3, 5, 3};

            student3.setGrades(numbers3);

        Student student4 = new Student("Dmitry", "Boytsov");
        int[] numbers4 = new int[]{3, 3, 3, 4, 3, 3, 5, 3};

            student4.setGrades(numbers4);

        Student student5 = new Student("Aleksey", "Azanov");
        int[] numbers5 = new int[]{3, 4, 5, 4, 5, 3};

            student5.setGrades(numbers5);

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        students = new Student[]{student, student2, student3, student4, student5};

        System.out.println("Студент с самым высоким средним баллом: " + bestStudent(students));

        System.out.println();
        System.out.println("До ===================================================================");
        System.out.println();

        Student[] checkBefore = students;
        for (Student s : checkBefore) {
            System.out.println(s);
        }

        sortBySurname(students);

        System.out.println();
        System.out.println("После ================================================================");
        System.out.println();

        Student[] checkAfter = students;
        for (Student s : checkAfter) {
            System.out.println(s);
        }
    }

    public static Student bestStudent(Student[] students) {
        double bestAVG = 0;
        double currentAVG;
        Student bestStudent = new Student();
        for (Student student : students) {
            currentAVG = student.gradesAVG();
            if(currentAVG > bestAVG) {
                bestAVG = currentAVG;
                bestStudent = student;
            }
        }
        return bestStudent;
    }

    public static void sortBySurname(Student[] students) {

        String[] surnames = new String[students.length];
        for (int i = 0; i < students.length; i++) {
            surnames[i] = students[i].getSurname();
        }

        Arrays.sort(surnames); // сортировка только фамилий

        Student[] indexes = new Student[students.length]; // массив для сопоставления индексов

        for (int i = 0; i < students.length; i++) {
            for (int j = 0; j < students.length; j++) {
                if (students[i].getSurname().equals(surnames[j])) {
                    indexes[j] = students[i];
                    break;
                }
            }
        }

        // Изменение исходного массива
        StudentService.students = Arrays.copyOf(indexes, indexes.length);
    }
}
