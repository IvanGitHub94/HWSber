package HW3_1;

public class TriangleChecker {
    public static void main(String[] args) {
        // false
        System.out.println(check(1, 1, 4));
        System.out.println(check(2, 3, 5));

        // true
        System.out.println(check(2, 5, 4));
        System.out.println(check(7, 3, 5));
    }

    public static boolean check(double a, double b, double c) {
        return a + b > c && a + c > b && b + c > a;
    }
}
