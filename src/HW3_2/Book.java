package HW3_2;

import java.util.Objects;

public class Book {
    private String author;
    private String title;
    private boolean isBorrowed = false;

    public Book(String author, String title) {
        validateAuthor(author);
        validateTitle(title);
    }

    public void setAuthor(String author) {
        validateAuthor(author);
    }

    public void setTitle(String title) {
        validateTitle(title);
    }

    public void setBorrowed(boolean borrowed) {
        this.isBorrowed = borrowed;
    }

    public String getAuthor() {
        return author;
    }

    public String getTitle() {
        return title;
    }

    public boolean isBorrowed() {
        return isBorrowed;
    }

    private void validateAuthor(String author) {
        try {
            if (author.length() < 1) {
                throw new AuthorLengthException();
            }
            else this.author = author;
        } catch (AuthorLengthException e) {
            System.out.println(e.getMessage());
        }
    }

    private void validateTitle(String title) {
        try {
            if (title.length() < 1) {
                throw new TitleLengthException();
            }
            else this.title = title;
        } catch (TitleLengthException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Автор: " + author + "\n" +
                "Книга: " + title + "\n" +
                "Одолжена: " + isBorrowed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return Objects.equals(title, book.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title);
    }
}

class AuthorLengthException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение author книги.";
    }
}
class TitleLengthException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение title книги.";
    }
}
