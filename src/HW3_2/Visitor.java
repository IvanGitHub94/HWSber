package HW3_2;

import java.util.Objects;

public class Visitor {
    private static int countOfIds = 1;
    private String name;
    private Integer id;

    private boolean hasBorrowedBook = false;

    public Visitor(String name) {
        validateName(name);
        this.id = null;
    }

    public void setName(String name) {
        validateName(name);
    }

    public void setId() {
        id = countOfIds++;
    }

    public void setHasBorrowedBook(boolean borrowedBook) {
        this.hasBorrowedBook = borrowedBook;
    }

    public static int getCountOfIds() {
        return countOfIds;
    }

    public String getName() {
        return name;
    }

    public Integer getId() {
        return id;
    }

    public boolean getHasBorrowedBook() {
        return hasBorrowedBook;
    }

    private void validateName(String name) {
        try {
            if (name.length() < 1) throw new NameLengthException();
            else this.name = name;
        } catch (NameLengthException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public String toString() {
        return "Visitor{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", borrowedBook=" + hasBorrowedBook +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Visitor visitor = (Visitor) o;
        return Objects.equals(name, visitor.name) && Objects.equals(id, visitor.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, id);
    }
}

class NameLengthException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение name посетителя.";
    }
}
class IdException extends Exception {
    @Override
    public String getMessage() {
        return "Неверное значение id посетителя.";
    }
}
