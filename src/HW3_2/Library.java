package HW3_2;

import java.util.ArrayList;
import java.util.HashSet;

public class Library {

    public static void main(String[] args) {
        Book book1 = new Book("Pushkin", "Tsar Soltan");
        Book book2 = new Book("Pushkin", "Gold fish");
        Book book3 = new Book("Pushkin", "Poems");
        Book book4 = new Book("Dostoevsky", "Idiot");
        Book book5 = new Book("Dostoevsky", "Player");

        HashSet<Book> bookHashSet = new HashSet<>();
        bookHashSet.add(book1);
        bookHashSet.add(book2);
        bookHashSet.add(book3);
        bookHashSet.add(book4);
        bookHashSet.add(book5);

        Visitor visitor1 = new Visitor("Petya");
        Visitor visitor2 = new Visitor("Leonid");
        Visitor visitor3 = new Visitor("Marina");
        Visitor visitor4 = new Visitor("Ira");

        ArrayList<Visitor> visitorsList = new ArrayList<>();
        visitorsList.add(visitor1);
        visitorsList.add(visitor2);
        visitorsList.add(visitor3);
        visitorsList.add(visitor4);


        Library library = new Library(bookHashSet);
        library.setVisitors(visitorsList);

        // Вывод всех книг
        /*for (Book b : library.getBooks()) {
            System.out.println(b);
        }*/

        System.out.println("_________________________________________________1");
        // ТЕСТЫ

        // Добавление книги
        library.addBook(new Book("Pushkin", "Gold fish"));
        library.addBook(new Book("Lermontov", "Mziry"));
        for (Book b : library.getBooks()) {
            System.out.println(b);
        }

        System.out.println("_________________________________________________2");
        // Удаление книги по названию

        library.removeBook("Mziry");
        library.removeBook("UnknownBook");
        for (Book b : library.getBooks()) {
            System.out.println(b);
        }

        System.out.println("_________________________________________________3");
        // Найти и вернуть книгу по названию
        System.out.println(library.findBook("UnknownBook"));
        System.out.println(library.findBook("Player"));

        System.out.println("_________________________________________________4");
        // Найти и вернуть список книг по автору
        for (Book b : library.booksOfAuthor("Mayakovsky")) {
            System.out.println(b);
        }
        for (Book b : library.booksOfAuthor("Dostoevsky")) {
            System.out.println(b);
        }

        System.out.println("_________________________________________________5");
        /*
            Одолжить книгу посетителю по названию, если выполнены все условия:
                a.	Она есть в библиотеке.
                b.	У посетителя сейчас нет книги.
                c.	Она не одолжена.
            Также если посетитель в первый раз обращается за книгой — дополнительно выдать ему идентификатор читателя.
        */
        Book testBook = new Book("Dostoevsky", "Player");
        Book testBook1 = new Book("Dostoevsky", "Besy");

        System.out.println("Книга есть в библиотеке? " + library.getBooks().contains(testBook));
        System.out.println("Книга есть в библиотеке? " + library.getBooks().contains(testBook1));

        System.out.println();

        System.out.println("    Посетитель одалживал книгу? " + visitor1.getHasBorrowedBook());
        System.out.println("    Id посетителя: " + visitor1.getId());
        System.out.println("    Книга одолжена? " + testBook.isBorrowed());
        System.out.println("*вызов метода одолжения книги*");
        library.borrowBookToVisitor("Player", visitor1); // "Player" - название book5
        for (Book b : library.getBooks()) {
            if(b.isBorrowed()) {
                System.out.println(b);
            }
        }
        System.out.println("    Посетитель одалживал книгу? " + visitor1.getHasBorrowedBook());
        System.out.println("    Id посетителя: " + visitor1.getId());

        System.out.println("_________________________________________________6");
        /*
            6.	Вернуть книгу в библиотеку от посетителя, который ранее одалживал книгу. Не принимать книгу от другого посетителя.
                a.	Книга перестает считаться одолженной.
                b.	У посетителя не остается книги.
        */

        library.returnBook(visitor1, book4);
        System.out.println();

        library.returnBook(visitor1, book5);
        for (Book b : library.getBooks()) {
            if(b.isBorrowed()) {
                System.out.println(b);
            }
        }
        System.out.println("Книга " + book5.getTitle() + " одолжена? " + book5.isBorrowed());

        library.returnBook(visitor3, book5);
    }
    private HashSet<Book> books;
    private ArrayList<Visitor> visitors;

    public Library(HashSet<Book> books) {
        this.books = books;
    }

    public void setBooks(HashSet<Book> books) {
        this.books = books;
    }

    public void setVisitors(ArrayList<Visitor> visitors) {
        this.visitors = visitors;
    }

    public HashSet<Book> getBooks() {
        return books;
    }

    public ArrayList<Visitor> getVisitors() {
        return visitors;
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public void removeBook(String title) {
        HashSet<Book> hashSet = new HashSet<>(books);
        for (Book b : books) {
            if(title.equals(b.getTitle()) && !b.isBorrowed()) {
                hashSet.remove(b);
            }
        }
        this.setBooks(hashSet);
    }

    public Book findBook(String title) {
        Book book = null;
        for (Book b : books) {
            if(title.equalsIgnoreCase(b.getTitle())) {
                book = b;
            }
        }
        return book;
    }

    public ArrayList<Book> booksOfAuthor(String author) {
        ArrayList<Book> bookArrayList = new ArrayList<>();
        for (Book b : books) {
            if(author.equalsIgnoreCase(b.getAuthor())) {
                bookArrayList.add(b);
            }
        }
        return bookArrayList;
    }

    public void borrowBookToVisitor(String title, Visitor visitor) {
        //visitors.add(visitor);
        if(!visitors.contains(visitor)) visitors.add(visitor);

        for (Book b : books) {
            if(title.equalsIgnoreCase(b.getTitle()) && !b.isBorrowed() && !visitor.getHasBorrowedBook()) {
                if (visitor.getId() == null) {
                    visitor.setId();
                }

                b.setBorrowed(true);
                visitor.setHasBorrowedBook(true);
            }
        }
    }

    public void returnBook(Visitor visitor, Book book) {
        if (!visitor.getHasBorrowedBook()) {
            System.out.println("Этот посетитель еще не одалживал книгу.");
            return;
        }
        if(!book.isBorrowed()) {
            System.out.println("Эта книга еще не была одолжена.");
        }
        else {
            for (Book b : books){
                if(b.equals(book)) {
                    b.setBorrowed(false);
                    visitor.setHasBorrowedBook(false);
                }
            }
        }
    }
}
